let newArray = [];

//Search field
const filteredArticle = async () => {
  var value = document.getElementById("search").value
  let filter = []
  const posts =   await mergedArray();
  let filterobj = {}
 posts.map(postitem => {
    if (postitem.title.includes(value)) {
      filterobj.title = postitem.title
      filterobj.id = postitem.id
      filterobj.body = postitem.body
      filterobj.thumbnailUrl = postitem.thumbnailUrl
      filter.push(filterobj)
    }
  });
  postComponent(filter);
};
  
document.getElementById("search").addEventListener("keyup",filteredArticle , false);

//Fetching all Posts
const getAllPosts = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  return res.json()
};

//Fetching all Photos
const getAllPhotos = async () => {
  const pic = await fetch('https://jsonplaceholder.typicode.com/photos')
  return pic.json();
};


//Mergin getAllPhotos and getAllPosts to a newArray
const mergedArray = async () => {
  newArray = [];
  const arrposts = await getAllPosts();
  const arrphotos = await getAllPhotos();
  arrposts.map(postitem => {
    var newobj = {};
     arrphotos.map(photoitem =>{
          if (photoitem.id === postitem.id){
            newobj.id = postitem.id
            newobj.title = postitem.title
            newobj.body = postitem.body
            newobj.url = photoitem.url
            newobj.thumbnailUrl = photoitem.thumbnailUrl
            newArray.push(newobj);
          }
      })
  })
  return newArray
};

// Creating the posts automaticaly
export const postComponent = async (posts) => {
  document.getElementById("postsRow").innerHTML = ""
  posts.map(post => {
    console.log(post.body.length)
    if (post.body.length > 150){
      post.body = post.body.substring(0, 150) + "..."
      console.log(post.body)
    }
       document.getElementById("postsRow").innerHTML += `
        <div class="col-sm-4">
          <div class="card" id="card-home" style="width: 24rem; height: 39em;">
            <img src="${post.thumbnailUrl}" class="card-img-top" alt="...">
              <div class="card-body" >
                <h5 style="height: 60px"class="card-title" id="title" >${post.title}</h5>
                <p style="height: 90px" id="text" class="card-text">${post.body}</p>
                <a href="pages/post.html?id=${post.id}" class="btn btn-primary" id="${post.id}">${post.id}</a>
              </div>
          </div>
        </div>
        `;
  });
}


//Main function
async function main() {
  const posts = await mergedArray();
  postComponent(posts);
}

main();

