//Get id of the Post
const getId = async () => {
    var queryString = window.location.search;
    queryString = queryString.slice(4, queryString.length);
    return queryString;
    
};

//Fetching all Posts
const getAllPosts = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  return res.json()
};

//Fetching all Photos
const getAllPhotos = async () => {
  const pic = await fetch('https://jsonplaceholder.typicode.com/photos')
  return pic.json();
};

//Get all comments
const getAllComments = async () => {
  const comments = await fetch('https://jsonplaceholder.typicode.com/comments')
  return comments.json();
};

//Mergin getAllPhotos and getAllPosts to a newArray
const mergedArray = async () => {
  let newArray = [];
  const arrposts = await getAllPosts();
  const arrphotos = await getAllPhotos();
  arrposts.map(postitem => {
    var newobj = {};
     arrphotos.map(photoitem =>{
          if (photoitem.id === postitem.id){
            newobj.id = postitem.id
            newobj.title = postitem.title
            newobj.body = postitem.body
            newobj.url = photoitem.url
            newobj.thumbnailUrl = photoitem.thumbnailUrl
            newArray.push(newobj);
          }
      })
  })
  return newArray
};

// Creating the posts automaticaly
const createArticle = async (article) => {
  document.getElementById("article-container").innerHTML = "";
  var id = await getId();
  var newarticle = article[id-1];
  document.getElementById("article-container").innerHTML += `
      <h1 class="title" id="title" >${newarticle.title}</h1>
      <img class="card-img" src="${newarticle.url}">
      <p id="text" class="text">${newarticle.body}</p>
    `;
};
// Pased on the ID it creates the Comments
const createComments = async () => {
  document.getElementById("comments").innerHTML = "";
  const comments = await getAllComments();
  var id = await getId();
  comments.map((com) =>{ 
    if (parseInt(com.postId) === parseInt(id)){
      document.getElementById("comments").innerHTML += `
      <div class="card">
        <div class="card-header">
          ${com.email}
        </div>
        <div class="card-body">
          <h4 class="card-title">${com.name}</h4>
          <p class="card-text">${com.body}</p>
        </div>
      </div>
      `;
    }
  });
}


    
async function main() {
  const article = await mergedArray();
    createArticle(article);
    createComments();
};
  
main();
  
//const filteredArray = async () => {
//   document.getElementById("article-container").innerHTML = "";
//   var value = document.getElementById("search").value
//   console.log(value);
//   let filter = []
//   const posts =   await mergedArray();
//   let filterobj = {}
//  posts.map(postitem => {
//     if (postitem.title.includes(value)) {
//       filterobj.title = postitem.title
//       filterobj.id = postitem.id
//       filterobj.body = postitem.body
//       filterobj.thumbnailUrl = postitem.thumbnailUrl
//       filter.push(filterobj)
//     }
//   })
//   console.log(filter)
//   document.getElementById("article-container").innerHTML += `
//       <h1 class="title" id="title" >${filter.title}</h1>
//       <img class="card-img" src="${filter.url}">
//       <p id="text" class="text">${filter.body}</p>
//     `;
// };

// document.getElementById("searchbtn").addEventListener("click",filteredArray , false);